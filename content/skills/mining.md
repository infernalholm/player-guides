---
title: 'Mining'
date: 2018-10-30T21:31:24Z
draft: false
---

<!-- vim-markdown-toc GitLab -->

* [Related Skills](#related-skills)
* [Related Items](#related-items)
* [Ores](#ores)
* [Mansory Resources](#mansory-resources)
* [How to raise](#how-to-raise)
  * [Macros](#macros)
  * [Script](#script)
  * [Instructions](#instructions)
* [Notes](#notes)

<!-- vim-markdown-toc -->

The Mining skill allows you to mine ores, and smelt them into ingots. The
higher your mining skill is, the better the ore you can mine, and better chance
you have to mine the rarer ores, and smelt them.

If you choose mining as your starting skill, you will be automatically issued a
Pick Axe, but you can also buy these from Blacksmith NPC's.

Once you have enough ore's, you can attempt smelting them into ingots, by
applying Coal to a Forge and then applying the Ore's to the forge.

While the percentage of mining a specific ore is random, the probabilities
change according to specific regions.

## Related Skills

- [Blacksmithy]({{<ref "blacksmithy.md">}})
- [Tinkering({{<ref "tinkering.md">}})
- [Masonry]({{<ref "masonry.md">}})

## Related Items

- Forge
- Pickaxe
- Shovel

## Ores

If you use a pickaxe to mine, you get the following:

| Ore       | % Mining | Location                     |
| --------- | -------- | ---------------------------- |
| Coal      | 0.5%     | North Britain mines          |
| Copper    | 2%       |                              |
| Iron      | 40%      |                              |
| Tin       | 45%      |                              |
| Ferrum    | 50%      |                              |
| Silver    | 70%      |                              |
| Gold      | 70%      |                              |
| Titanium  | 70%      |                              |
| Terathane | 75%      | Terathane Keep and others    |
| Shadow    | 75%      |                              |
| Agapite   | 75%      |                              |
| Verite    | 75%      |                              |
| Valorite  | 80%      |                              |
| Skargard  | 95%      | Deceit dungeon on Ice Island |
| Mytheril  | 100%     |                              |
| Blackrock | 100%     |                              |

## Mansory Resources

If you use a shovel to mine, you get the following:

| Ore       | % Mining Required |
| --------- | --------          |
| Sandstone | 85%               |
| Stone     | 85%               |
| Granite   | 90%               |
| Gypsum    | ?%                |
| Clay      | 90%               |

## How to raise

### Macros

`F1`: Last Object(Pickaxe), Last Target (Floor)
`F2`: Last Object (Pickaxe)
`F6`: Walk from Y1 to Y2, Y3, Y4, Y5 (Macro must have **one** walk. Either “walk w” or “walk n” or “walk ne” etc. DISABLE ALWAYS RUN.)
`F4`: Walk from Y5 to Y1 (On the same in game macro you should have for example “walk ws walk ws walk ws walk ws” 4 walks)

Locations #1,2,3,4 – the first 4 locations you can reach without move
Locations #5,6,7 – the 3 locations you will be mining as you move

### Script

```
PRESS KEY F2
LEFTCLICK Location #1
PAUSE 10000
LOOP 2
PRESS KEY F1
PAUSE 10000
END LOOP
PRESS KEY F2
LEFTCLICK Location #2
PAUSE 10000
LOOP 2
PRESS KEY F1
PAUSE 10000
END LOOP
PRESS KEY F2
LEFTCLICK Location #3
PAUSE 10000
LOOP 2
PRESS KEY F1
PAUSE 10000
END LOOP
PRESS KEY F2
LEFTCLICK Location #4
PAUSE 10000
LOOP 2
PRESS KEY F1
PAUSE 10000
END LOOP
LOOP 3
PRESS KEY F6
PAUSE 1000
PRESS KEY F2
LEFTCLICK Location #5
PAUSE 10000
LOOP 2
PRESS KEY F1
PAUSE 10000
END LOOP
PRESS KEY F2
LEFTCLICK Location #6
PAUSE 10000
LOOP 2
PRESS KEY F1
PAUSE 10000
END LOOP
PRESS KEY F2
LEFTCLICK Location #7
PAUSE 10000
LOOP 2
PRESS KEY F1
PAUSE 10000
END LOOP
END LOOP
PRESS KEY F4
PAUSE 1000
```

### Instructions

You will need to find a place like this:

```
______1___________5_____________5___________5____________5____

_2____Y1___4______Y2____7_______Y3___7______Y4____7______Y5____7___

______3___________6_____________6____________6____________6____
```

1/2/3/4/5/6/7 – Locations where you are going to mine
Y1 – Where you start the macro
Y2-Y5 – Places where you will be moving to as you mine

The locations are not randomly set.  
As you can see, when you finish mining the first 4 locations you will turn to
Y2. This way you will not “waste” any step of your macro. 
When you finish mining at location Y5, you will turn to location 7 (the one at
the right), so you need one more "walk" on your F4 macro to back to location Y1
again.

## Notes

- Mt. Kendal (Minoc) is a good place to start with mining, but it produces
  nothing but copper and iron. To mine better ores you must use the mines away
  from towns.
- Certain ores are exclusive to certain areas of the map.
- Skargard is exclusive to Deceit dungeon, on Ice Island.
- When mining if your skill is high enough, you have a chance to sometimes mine
  gems.
  
{{% notice warning %}} Attacking player's steeds in Mount Kendal is an offence that
grants you jail time. {{% /notice  %}}
