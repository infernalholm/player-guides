+++
title= "Taming"
date= 2018-10-31T19:33:49Z
draft= false
description= "Taming. The n-legged animals whisperer"
+++

<!-- vim-markdown-toc GitLab -->

* [Related Skills](#related-skills)
* [Related Items](#related-items)
* [Horses](#horses)
* [Mustangs](#mustangs)
* [Llamas](#llamas)
* [Ostards](#ostards)
* [Notes](#notes)

<!-- vim-markdown-toc -->

Players can get control over wild animals, and turn them into pets, with the
Taming skill.

You should know that some spawn points are hard to access.
Mobs, that you'll have to clear through, will surround certain spawn points.

Also, what may have spawned there the first time you saw the spawn may not spawn
there the next time you visit.

The system works on chance to make certain steeds that little bit rarer and more
collectible.

You may get Taming lessons from Animal Tamer NPCs found in most towns

## Related Skills

- Creature Lore
- [Inscription]({{<ref "inscription.md" >}})
- [Alchemy]({{<ref "alchemy.md">}})

## Related Items

- Shrink Potion

## Horses

| Name             | Taming | Creature Lore | Damage Bonus | Dodge Bonus |
| ---------------- | ------ | ------------- | ------------ | ----------- |
| Horse            | 45%    | 0%            | 0%           | 0%          |
| Jungle Steed     | 55%    | 40%           | 4%           | 8%          |
| Holy Steed       | 50%    | 30%           | 3%           | 6%          |
| Evil Steed       | 90%    | 30%           | 3%           | 6%          |
| Moon Steed       | 60%    | 60%           | 6%           | 12%         |
| Ice Horse        | 75%    | 40%           | 4%           | 8%          |
| Ethereal Horse   | 100%   | 50%           | 5%           | 10%         |
| Shadow           | 100%   | 30%           | 3%           | 6%          |
| Undead Horse     | 70%    | 70%           | 7%           | 14%         |
| Nightmare        | 100%   | 60%           | 6%           | 12%         |
| Unicorn          | 100%   | 70%           | 7%           | 14%         |
| Fire Steed       | 100%   | 60%           | 6%           | 12%         |
| Steed of Sand    | 85%    | 40%           | 8%           | 13%         |
| War Horse        | ?%     | 90%           | 18%          | 30%         |
| Steed Of The Sun | ?%     | 30%           | 6%           | 10%         |
| Packhorse        | 45%    | n/a           | n/a          | n/a         |

## Mustangs

| Name              | Taming | Creature Lore | Damage Bonus | Dodge Bonus |
| ----------------- | ------ | ------------- | ------------ | ----------- |
| Grey Mustang      | 50%    | 25%           | 2%           | 5%          |
| Ruan Mustang      | 52%    | 26%           | 2%           | 5%          |
| Chocolate Mustang | 54%    | 27%           | 2%           | 5%          |
| Red Ruan Mustang  | 56%    | 28%           | 2%           | 5%          |
| Sky Blue Mustang  | 58%    | 29%           | 2%           | 5%          |
| Pamamino Mustang  | 60%    | 30%           | 3%           | 6%          |
| Wimmimate Mustang | 62%    | 31%           | 3%           | 6%          |
| Sky Grey Mustang  | 64%    | 32%           | 3%           | 6%          |
| Crimson Mustang   | 66%    | 30%           | 3%           | 6%          |
| Black Mustang     | 68%    | 34%           | 3%           | 6%          |
| Warped Mustang    | 65%    | 25%           | 7%           | 14%         |

## Llamas

| Name              | Taming | Creature Lore | Damage Bonus | Dodge Bonus |
| ----------------- | ------ | ------------- | ------------ | ----------- |
| Llama             | 60%    | 10%           | 1%           | 2%          |
| Sky Blue Llama    | 66%    | 35%           | 3%           | 7%          |
| Chocolate Llama   | 65%    | 35%           | 3%           | 7%          |
| Crimson Llama     | 65%    | 35%           | 3%           | 7%          |
| Ruan Llama        | 65%    | 35%           | 3%           | 7%          |
| Black Llama       | 66%    | 35%           | 3%           | 7%          |
| Pamamino Llama    | 65%    | 35%           | 3%           | 7%          |
| Red Ruan Llama    | 65%    | 35%           | 3%           | 7%          |
| Grey Llama        | 65%    | 35%           | 3%           | 7%          |
| Angelic Llama     | 80%    | 40%           | 4%           | 8%          |
| Ethereal Llama    | 90%    | 40%           | 4%           | 8%          |
| Llama Of The Moon | 80%    | 40%           | 4%           | 8%          |

## Ostards

| Name                 | Taming | Creature Lore | Damage Bonus | Dodge Bonus |
| -------------------- | ------ | ------------- | ------------ | ----------- |
| Orn                  | 60.0%  | 30%           | 5%           | 2%          |
| Zostrich             | 70.0%  | 40%           | 6%           | 3%          |
| Terathane Zostrich   | ?%     | 40%           | 6%           | 13%         |
| Dark Zostrich        | ?%     | 80%           | 12%          | 6%          |
| Jungle Orn           | 55.0%  | 50%           | 8%           | 4%          |
| Lava Zostrich        | 70.0%  | 40%           | 6%           | 3%          |
| Ice Zostrich         | 75.0%  | 40%           | 6%           | 3%          |
| Ethereal Orn         | 95.0%  | 50%           | 8%           | 4%          |
| Ethereal Zostrich    | 100.0% | 50%           | 8%           | 4%          |
| Death Steed          | 85.0%  | 30%           | 5%           | 2%          |
| Void Zostrich        | 100.0% | 60%           | 10%          | 5%          |
| Angelic Oclock       | 85.0%  | 40%           | 6%           | 3%          |
| Angelic Orn          | 85.0%  | 40%           | 6%           | 3%          |
| Angelic Zostrich     | 95.0%  | 40%           | 6%           | 3%          |
| Balron Zostrich      | ?%     | 50%           | 8%           | 4%          |
| Oclock Of The Moon   | 85.0%  | 65%           | 12%          | 5%          |
| Orn Of The Moon      | 90.0%  | 75%           | 12%          | 6%          |
| Zostrich Of The Moon | 95.0%  | 85%           | 14%          | 7%          |
| Experimental Steed   | ?%     | 100%          | 5%           | 2%          |

## Notes

- We refuse to tell players where specific steeds spawn as that would take the
  fun/adventurer spirit out of the mount system.
- You have to have, at least, the specified Create Lore skill in your character
  to have access to the specified bonuses.
- Damage and dodge bonus affects melee attacks, while riding the creature.
- Not all displayed steeds are spawning at the moment.

Steed books have to ability to store all spawned mounts.
There are specific books for mounts, and also a standard book which can hold a
set amount of mixed creatures.

Please bare in mind although not enforced, it's preferred by the staff team that
players with large amounts of mounts, or pets, take advantage of these books and
use them for storage of excess mounts, pets, rather than having 200+ pets
displayed.
