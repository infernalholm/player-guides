---
title: 'Masonry'
date: 2018-11-01T19:28:09Z
draft: false
---

<!-- vim-markdown-toc GitLab -->

* [Related Skills](#related-skills)
* [Related Items](#related-items)
* [Craftables](#craftables)
  * [House Parts](#house-parts)
  * [Furniture](#furniture)
  * [Decoration](#decoration)
  * [House Plans](#house-plans)

<!-- vim-markdown-toc -->

Craft usable items from different types of stone. Such items include benches and
tables and even houses.

## Related Skills

- Mining

## Related Items

- Masonry Tools

## Craftables

{{% notice note %}}
Remember that you need [Mining]({{<ref "mining.md#mansory-resources">}}) to
gather resources for masonry.
{{% /notice %}}

### House Parts

| Craftable           | Resources                     | Skill |
| ------------------- | ----------------------------- | ----- |
| Brick               | 10 Sandstone, 10 Clay, 5 Coal | 50%   |
| Sandstone Housepart | 20 Sandstone                  | 60%   |
| Stone Housepart     | 20 Stone                      | 75%   |
| Brick Housepart     | 20 Brick                      | 75%   |
| Granite Housepart   | 20 Granite                    | 80%   |

### Furniture

| Item            | Resources    | Skill |
| --------------- | ------------ | ----- |
| Sandstone Chair | 15 Sandstone | 60%   |
| Sandstone Bench | 10 Sandstone | 60%   |
| Sandstone Table | 20 Sandstone | 60%   |
| Stone Chair     | 15 Stone     | 70%   |
| Stone Bench     | 10 Stone     | 70%   |
| Stone Table     | 20 Stone     | 70%   |
| Granite Chair   | 15 Granite   | 80%   |
| Granite Bench   | 10 Granite   | 80%   |
| Granite Table   | 20 Granite   | 80%   |

### Decoration

| Item                | Resources   | Skill |
| ------------------- | ----------- | ----- |
| Sandstone Pedestal  | 5 Sandstone | 30%   |
| Sandstone Statue(s) | 3 Sandstone | 30%   |
| Sandstone Carving   | 8 Sandstone | 50%   |
| Stone Pedestal      | 5 Stone     | 40%   |
| Stone Statue(s)     | 3 Stone     | 40%   |
| Stone Carving       | 8 Stone     | 60%   |
| Granite Pedestal    | 5 Granite   | 30%   |
| Granite Statue(s)   | 3 Granite   | 30%   |
| Granite Carving     | 8 Granite   | 70%   |

### House Plans

| Item                  | Resources                                                       | Skill |
| --------------------- | ------------------------------------------------------------    | ----- |
| Small Stone House     | 50 Stone Houseparts 5 Tinker Houseparts 5 Carpentry Houseparts  | 85%   |
| Small Brick House     | 50 Brick Houseparts 5 Tinker Houseparts 5 Carpentry Houseparts  | 85%   |
| Small Wood House      | 25 Stone Houseparts 5 Tinker Houseparts 10 Carpentry Houseparts | 85%   |
| Sandstone Patio House | 100 Sandstone Houseparts 10 Tinker Parts 10 Carpentry Parts     | 95%   |
| Spy Tower             | 75 Granite Houseparts 10 Tinker Parts 10 Carpentry Parts        | 95%   |
| Wizards Tower         | 100 Granite Houseparts 20 Tinker Parts 20 Carpentry Parts       | 100%  |
