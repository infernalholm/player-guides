+++
title="Tailoring"
date = "2018-10-30T20:13:42+00:00"
description = "Tailoring. Used to create clothes, apply dyes and leather armors"
+++

<!-- vim-markdown-toc GitLab -->

* [Related Skills](#related-skills)
* [Related Items](#related-items)
* [Resources](#resources)
* [Craftables](#craftables)

<!-- vim-markdown-toc -->

## Related Skills

- [Arms Lore]({{< ref "arms-lore.md" >}})

## Related Items

| Item           | How can it acquired                                                    |
| -------------- | ---------------------------------------------------------------------- |
| Dyes           | Purchased from NPC Tailors                                             |
| Dye Tubs       | Purchased from NPC Tailors                                             |
| Furs/Hides     | Obtained by skinning dead animals or monsters                          |
| Feathers       | Obtained by skinning dead animals or monsters                          |
| Ingots         | Crafted by Blacksmiths. NPC Tinkers and Blacksmiths also sell some     |
| Logs           | Retrieved via [Lumberjacking]({{<ref "lumberjacking.md">}})            |
| Loom           | Purchased for 50k by paging a GM in game. Tailoring shops also have it |
| Rare Dyes      | Crafted by Alchemists                                                  |
| Scissors       | Purchased from NPC Tailers or crafted by Tinkers                       |
| Sew kit        | Purchased from NPC Tailors                                             |
| Spinning Wheel | Crafted by Carpenters                                                  |

## Resources

| Resource           | Tailoring % | Double-Click         | Target         |
| ------------------ | ----------- | -------------------- | -------------- |
| Bandages           | 0           | Scissors             | Folded Cloth   |
| Folded Cloth       | 15          | Scissors             | Bolt of Cloth  |
| Colored Cloth      | 15          | Dye Tub              | Clothes        |
| Spool of Thread    | 30          | Cotton               | Spinning Wheel |
| Ball of Yarn       | 25          | Wool                 | Spinning Wheel |
| Cut Hides          | 30          | Scissors             | Furs or Hides  |
| Bolt of Cloth      | 40          | Spools of Thread     | Upright Loom   |
| Bolt of Cloth      | 40          | Balls of Yarn        | Upright Loom   |
| Rare Colored Cloth | 70          | Rare Dye Tub         | Clothes        |
| Hardened Leather   | 79          | Soft Copper Acid Vat | Cut Hides      |

## Craftables

| Leather Item                        | Resources\*                                   | Tailoring % | Arms Lore % | Bonus                                   |
| ----------------------------------- | --------------------------------------------- | ----------- | ----------- | --------------------------------------- |
| Pouch                               | 1 CL                                          | 5           | 0           |                                         |
| Leather Cap                         | 2 CL                                          | 6.8         | 0           |                                         |
| Sandals                             | 4 CL                                          | 10          | 0           |                                         |
| Leather Bag                         | 1 CL                                          | 10          | 0           |                                         |
| Backpack                            | 4 CL                                          | 20          | 0           |                                         |
| Tribal Mask                         | 1 CL, 4 cloth, 1 dye                          | 20          | 0           |                                         |
| Tribal Mask 2                       | 1 CL, 4 cloth, 1 dye                          | 21          | 0           |                                         |
| Shoes                               | 6 CL                                          | 31          | 0           |                                         |
| Bear Mask                           | 2 CL, 2 cloth, 1 pine log                     | 40          | 0           |                                         |
| Deer Mask                           | 2 CL, 2 cloth, 1 pine log                     | 41          | 0           |                                         |
| Orc Helm                            | 2 CL, 4 cloth, 1 pine log                     | 50          | 0           |                                         |
| Leather Gorget                      | 4 CL                                          | 51          | 25          |                                         |
| Boots                               | 8 CL                                          | 55          | 0           |                                         |
| Leather Gloves                      | 3 CL                                          | 56          | 27          |                                         |
| Female Leather Skirt                | 6 CL                                          | 58          | 26          |                                         |
| Female Leather Bustier              | 4 CL                                          | 58.5        | 26          |                                         |
| Orc Mask                            | 2 CL, 4 cloth, 1 pine log                     | 60          | 0           |                                         |
| Leather Sleeves                     | 8 CL                                          | 60          | 30          |                                         |
| Thigh Boots                         | 10 CL                                         | 61          | 0           |                                         |
| Female Leather Shorts               | 4 CL                                          | 62.2        | 31          |                                         |
| Female Leather Armor                | 8 CL                                          | 62.2        | 31          |                                         |
| Studded Gorget                      | 6 CL, 1 iron ingot                            | 65          | 32          |                                         |
| Leather Leggings                    | 10 CL                                         | 65          | 32          |                                         |
| Studded Gloves                      | 8 CL, 1 iron ingot                            | 70          | 35          |                                         |
| Leather Tunic                       | 12 CL                                         | 70.5        | 35          |                                         |
| Studded Sleeves                     | 10 CL, ? iron ingot                           | 75          | 37          |                                         |
| Studded Leggings                    | 12 CL, 2 iron ingots                          | 80          | 40          |                                         |
| Female Studded Bustier              | 4 CL, 1 iron ingot                            | 83          | 41          |                                         |
| Female Studded Armor                | 10 CL, 1 iron ingot                           | 84          | 42          |                                         |
| Studded Tunic                       | 14 CL, 3 iron ingots                          | 85          | 42          |                                         |
| Ranger Leather Gloves               | 8 HL, 1 steel ingot, 1 dye                    | 85.5        | 42          | +1.0 Archery, +1.0 Hiding, +1.0 Stealth |
| Ranger Leather Gorget               | 4 HL, 1 steel ingot, 1 dye                    | 87.5        | 40.5        | +0.5 Archery, +0.5 Hiding, +0.5 Stealth |
| Ranger Leather Arms                 | 10 HL, 1 steel ingot, 1 dye                   | 88.5        | 44          | +1.5 Archery, +1.5 Hiding, +1.5 Stealth |
| Ranger Leather Leggings             | 10 HL, 1 steel ingot, 1 dye                   | 92.5        | 46          | +3.0 Archery, +3.0 Hiding, +3.0 Stealth |
| Ranger Leather Tunic                | 12 HL, 1 steel ingot, 1 dye                   | 95.5        | 48          | +4.0 Archery, +4.0 Hiding, +4.0 Stealth |
| Female Ranger Leather Armor         | 10 HL, 1 steel ingot, 1 dye                   | 90          | 45          | +3.0 Archery, +3.0 Hiding, +3.0 Stealth |
| Drake Cap                           | 2 UDH, 1 steel ingot, 1 spools of thread      | 88          | 50          |                                         |
| Drake Gloves                        | 3 UDH, 1 steel ingot, 1 spools of thread      | 88          | 50          |                                         |
| Drake Gorget                        | 4 UDH, 2 steel ingot, 2 spools of thread      | 88          | 50          |                                         |
| Drake Arms                          | 4 UDH, 2 steel ingots, 2 spools of thread     | 88          | 50          |                                         |
| Female Drake Armor                  | 12 UDH, 6 steel ingots, 6 spools of thread    | 89.2        | 50          |                                         |
| Drake Leggings                      | 10 UDH, 4 steel ingots, 4 spools of thread    | 89.2        | 50          |                                         |
| Drake Tunic                         | 12 UDH, 6 steel ingots, 6 spools of thread    | 89.2        | 50          |                                         |
| Terathane Scale Gloves              | 8 HL, 8 zostrich scales, 2 spools of thread   | 88.5        | 42          | +1.0 Magic Resistance                   |
| Terathane Scale Gorget              | 4 HL, 4 zostrich scales, 2 spools of thread   | 91.5        | 40.5        | +0.5 Magic Resistance                   |
| Terathane Scale Arms                | 10 HL, 10 zostrich scales, 2 spools of thread | 93.5        | 44          | +1.5 Magic Resistance                   |
| Terathane Scale Leggings            | 10 HL, 10 zostrich scales, 4 spools of thread | 97.5        | 46          | +3.0 Magic Resistance                   |
| Terathane Scale Tunic               | 12 HL, 12 zostrich scales, 4 spools of thread | 100         | 48          | +4.0 Magic Resistance                   |
| Female Terathane Scale Armor        | 10 HL, 10 zostrich scales                     | 95.5        | 45          | +3.0 Magic Resistance                   |
| Red Dragon Leather Cap              | 2 UDH, 1 steel ingot, 1 spool of thread       | 98.0        | 50          |                                         |
| Red Dragon Leather Cap              | 4 UDH, 2 steel ingot, 2 spool of thread       | 98.0        | 50          |                                         |
| Red Dragon Leather Gloves           | 3UDH, 1 steel ingot, 1 spool of thread        | 98.0        | 50          |                                         |
| Red Dragon Leather Sleeves          | 4 UDH, 2 steel ingot, 2 spool of thread       | 98.0        | 50          |                                         |
| Red Dragon Leather Leggings         | 10 UDH, 4 steel ingot, 4 spool of thread      | 99.2        | 50          |                                         |
| Red Dragon Leather Tunic            | 12 UDH, 6 steel ingot, 6 spool of thread      | 99.2        | 50          |                                         |
| Red Dragon Leather Female Tunic     | 120 UDH, 6 steel ingot, 6 spool of thread     | 99.2        | 50          |                                         |
| Green Dragon Leather Cap            | 2 UDH, 1 steel ingot, 1 spool of thread       | 98.0        | 50          |                                         |
| Green Dragon Leather Cap            | 4 UDH, 2 steel ingot, 2 spool of thread       | 98.0        | 50          |                                         |
| Green Dragon Leather Gloves         | 3UDH, 1 steel ingot, 1 spool of thread        | 98.0        | 50          |                                         |
| Green Dragon Leather Sleeves        | 4 UDH, 2 steel ingot, 2 spool of thread       | 98.0        | 50          |                                         |
| Green Dragon Leather Leggings       | 10 UDH, 4 steel ingot, 4 spool of thread      | 99.2        | 50          |                                         |
| Green Dragon Leather Tunic          | 12 UDH, 6 steel ingot, 6 spool of thread      | 99.2        | 50          |                                         |
| Green Dragon Leather Female Tunic   | 12 UDH, 6 steel ingot, 6 spool of thread      | 99.2        | 50          |                                         |
| Crystal Dragon Leather Cap          | 4 UDH, 2 steel ingot, 2 spool of thread       | 99.9        | 50          |                                         |
| Crystal Dragon Leather Gloves       | 3 UDH, 1 steel ingot, 1 spool of thread       | 99.9        | 50          |                                         |
| Crystal Dragon Leather Sleeves      | 4 UDH, 2 steel ingot, 2 spool of thread       | 99.9        | 50          |                                         |
| Crystal Dragon Leather Leggings     | 10 UDH, 4 steel ingot, 4 spool of thread      | 99.9        | 50          |                                         |
| Crystal Dragon Leather Tunic        | 12 UDH, 6 steel ingot, 6 spool of thread      | 99.9        | 50          |                                         |
| Crystal Dragon Leather Female Tunic | 12 UDH, 6 steel ingot, 6 spool of thread      | 99.9        | 50          |                                         |

**NB**: \* CL=Cut Leather; HL=Hardened Leather; UDH = Uncut Dragon Hide
