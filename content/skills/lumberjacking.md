---
title: 'Lumberjacking'
date: 2018-11-01T19:09:03Z
draft: false
---

<!-- vim-markdown-toc GitLab -->

* [Related Skills](#related-skills)
* [Related Items](#related-items)
* [Logs](#logs)

<!-- vim-markdown-toc -->

Chop trees and gather different types of logs to make wooden furniture and
weapons.

## Related Skills

- [Carpentry]({{<ref "carpentry.md">}})
- [Bowery]({{<ref "bowery.md">}})

## Related Items

- Axe

## Logs

| Wood       | Skill |
| ---------- | ----- |
| Pine       | 35%   |
| Eucalyptus | 45%   |
| Holly      | 55%   |
| Rose       | 55%   |
| Oak        | 65%   |
| Ironwood   | 70%   |
| Mahogany   | 85%   |
| Cherry     | 95%   |
