---
title: 'Cartography'
date: 2018-10-30T20:34:19Z
draft: false
description: Cartography. Decipher maps and go treasure hunting with traders.
---

<!-- vim-markdown-toc GitLab -->

* [Related Skills](#related-skills)
* [Related Items](#related-items)
* [Levels](#levels)
* [Tips](#tips)

<!-- vim-markdown-toc -->

You can raise this skill by making maps. You can buy blank maps from
Cartography NPC's, 3 of wich are in Britain, 1 in Vesper and other in Skara
Brae.

At 75% you can make full world maps

## Related Skills

| Skill                               | Required for                                                                                                             |
| ----------------------------------- | ------------------------------------------------------------------------------------------------------------------------ |
| [Mining]({{< ref "mining.md" >}})   | Used to dig up treasure                                                                                                  |
| [Fishing]({{< ref "fishing.md" >}}) | You can fish Treasure Maps with this skill, tho they are always lvl 1(You can fish them up from 50% Fishing and upwards) |

## Related Items

- Pick Axe
- Fishing Rod

## Levels

To dig up and decipher a map you'll need the follow % of skill:

| Map Level | Mining | Cartography | Expected Monsters                                                           |
| --------- | ------ | ----------- | --------------------------------------------------------------------------- |
| 1         | 50%    | 50%         | Skeletons, Orc Captains, Ettins & Trolls                                    |
| 2         | 65%    | 65%         | Gazers, Hell Hounds, Gargoyles, Orc mages & Liches                          |
| 3         | 75%    | 75%         | Ogre Lords, Liche Lords, Elder Gazers, Fire Elementals, Water Elementals    |
| 4         | 90%    | 90%         | Daemons, Greater Daemons, Poison Elementals, Blood Elementals & Liche Lords |
| 5         | 100%   | 100%        | Daemons, Greater Daemons, Poison Elementals, Blood Elementals, Balrons      |

## Tips

When you have deciphered a map, you'll have to find its location.

The best way to do this is use the UO Auto Map program. Go to the display
options and turn Tilt off, so that the landscape will look the same as it does
on the treasure maps so it won't be hard to locate them.

When you've located the spot, get a Miner with the appropriate % of mining, go
to the location, hold a pickaxe in hand, and double click the plants around you
till you dig it up. In other words, you don't mine the floor. You double click
the plants while holding the pickaxe.

This process can take anywhere from 5 minutes to an hour or more depending on
the plants at said location and your luck.
