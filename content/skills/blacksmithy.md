---
title: 'Blacksmithy'
date: 2018-11-01T16:33:47Z
draft: false
---

<!-- vim-markdown-toc GitLab -->

* [Related Skills](#related-skills)
* [Related Items](#related-items)
* [Resources](#resources)
  * [Ingots](#ingots)
  * [Alloys](#alloys)
* [Bonuses](#bonuses)
* [Skill Modifiers](#skill-modifiers)
* [Craftables](#craftables)
  * [Armour](#armour)
  * [Shields](#shields)
  * [Fencing Weapons](#fencing-weapons)
  * [Blade Weapons](#blade-weapons)
  * [Blunt Weapons](#blunt-weapons)

<!-- vim-markdown-toc -->

Blacksmithy is one of the most profitable skills. It allows one to craft both
armour and weapons of high quality in different metals, some with unique
gameplay affecting effects.

## Related Skills

- [Arms Lore]({{<ref "arms-lore.md">}})
- [Mining]({{<ref "mining.md">}})
- [Tinkering]({{<ref "tinkering.md">}})

## Related Items

- Forge
- Coal
- Blacksmith's Hammer

## Resources

Before you can make anything, you will need ingots, obtained by smelting ore in
a forge, as well as coal.

### Ingots

| Ingot       | Resources         |
| ----------- | ----------------- |
| Copper      | Copper ore        |
| Pure copper | Pure copper ore   |
| Old copper  | Old copper ore    |
| Tin         | Tin ore           |
| Iron        | Iron ore          |
| Ferrum      | Ferrum ore        |
| Silver      | Silver ore        |
| Gold        | Gold ore          |
| Titanium    | Titanium ore      |
| Agapite     | Agapite ore       |
| Shadow      | Shadow ore        |
| Verite      | Verite ore        |
| Valorite    | Valorite ore      |
| Terathane   | Terathane ore     |
| Mythril     | Mythril ore       |
| Skargard    | Skargard crystals |
| Blackrock   | Blackrock ore     |

### Alloys

Combinations of metals and/or other resources.

The ability to create an alloy will appear in the item creation menu alongside
then normal shields, etc options when you have both the skill and the metals to
create alloys.

You do require coal in your backpack when making alloys, but most alloys don't
consume it.

{{% notice note %}}
Mining is necessary for crafting alloys from resources.
{{% /notice %}}

| Alloy             | Resources                                                                                  | Mining | Blacksmithy | Ingots Produced           |
| ----------------- | ------------------------------------------------------------------------------------------ | ------ | ----------- | ------------------------- |
| Brass             | Tin ingots, Pure copper ingots                                                             | 60%    | 20%         | 4 Brass ingots            |
| Steel             | 10 Iron ingots, 8 Coal                                                                     | 65%    | 30%         | 4 Steel ingots            |
| Chrome            | Tin ingots, Ferrum ingots                                                                  | 70%    | 30%         | 4 Chrone ingots           |
| Stronghold        | 4 Titanium ingots, 2 Tin ingots, 1 Angel Dust                                              | 85%    | 30%         | 4 Stronghold ingots       |
| Verminard         | 4 Terathane ingots, 2 Ferrum ingots, 2 Tin ingots, 1 Black Gem                             | 85%    | 30%         | 4 Verminard ingots        |
| Adamantine        | 6 Titanium ingots, 2 Tin ingots, 2 Iron ingots, 4 Diamonds                                 | 85%    | 40%         | 4 Adamantine ingots       |
| Phoenix           | 1 Phoenix feather, Gold ingots, Sulphurous ash, Tin ingots                                 | 90%    | 40%         | 1 Phoenix ingot           |
| Bloodrock         | 2 Blackrock ingots, 1 Blood spawn, 1 Blood vial, 5 Ginseng                                 | ?      | ?           | 1 Bloodrock ingot         |
| Enriched Titanium | 6 Greater mana potion, 4 Dispel scrolls, 4 Reflect scrolls, 1 Titanium ingot, 1 Gold ingot | ?      | ?           | 1 Enriched Titanium ingot |

## Bonuses

| Ingot/Alloy       | Armour Effect                                                   | Weapon Effect                         | Notes                                                                                                                      |
| ----------------- | --------------------------------------------------------------- | ------------------------------------- | -------------------------------------------------------------------------------------------------------------------------- |
| Agapite           | Gods protection, randomly nullifies melee or Magery attacks     | Purifying, damage based on kill/karma | requirements ( blue )                                                                                                      |
| Shadow            | Shadow protection, identical to Agapite ( see above )           | Tainting, damage based on kill/karma  | requirements ( red )                                                                                                       |
| Verite            | Mana regeneration, restores mana periodically                   | Mana Vampire                          |                                                                                                                            |
| Valorite          | Stamina regeneration, restores stamina periodically             | Stamina Vampire                       |                                                                                                                            |
| Bloodrock         | Health regeneration, restores health periodically               | Health vampire                        |                                                                                                                            |
| Mythril           | yields high amount of AR                                        | not available                         | Can't create weapons                                                                                                       |
| Stronghold        | Retribution (reactive armour like effect), protection vs daemon | Daemon/Undead slaying                 | formerly the official Holy Knights equipment                                                                               |
| Verminard         | Vengeance, protection vs shadows                                | Shadow bane                           | formerly the official Dark Knights equipment                                                                               |
| Skargard          | Phasing                                                         | Cold damage/Stamina drain             | Smelting has to happen in Deceit Dungeon                                                                                   |
| Phoenix           | Rebirth                                                         | Fire damage                           | Feathers acquired by killing the Phoenix                                                                                   |
| Enriched titanium | Spell absorption                                                | not available                         | Spell absorption includes your own spells and potions. Will offer full immunity when wearing full plate with heater shield |

## Skill Modifiers

When crafting an item, add the following modifier to the required skill in order
to get the skill amount necessary to craft the item.

{{% notice tip %}}
To create gold ringmail gloves, you need at least 45.8% of Blacksmithy skill.
25.8% for ringmail gloves plus 20% for the metal modifier.
{{% /notice %}}

| Metal       | Modifier |
| ----------- | -------- |
| Copper      | 0%       |
| Pure Copper | 0%       |
| Chrome      | +7%      |
| Ferrum      | +7%      |
| Iron        | +7%      |
| Brass       | +12%     |
| Bronze      | +12%     |
| Steel       | +16.5%   |
| Silver      | +20%     |
| Gold        | +20%     |
| Titanium    | +29%     |
| Adamantine  | +29%     |
| Skargard    | +29%     |
| Verminard   | +29%     |
| Stronghold  | +29%     |
| Blackrock   | +29%     |
| Bloodrock   | +29%     |
| Phoenix     | +29%     |

## Craftables

{{% notice note %}}
Arms lore is necessary to craft armour and weapons.
To identity the crafted items, you can use either [Arms Lore]({{<ref "arms-lore.md">}}) or Item Lore.
{{% /notice %}}

{{% notice note %}}
Don't forget to check the Skill modifier for the material you're using.
{{% /notice %}}

### Armour

| Item               | Ingots | Skill |
| ------------------ | ------ | ----- |
| Ringmail gloves    | 10     | 25.8% |
| Ringmail leggings  | 16     | 32.8% |
| Chainmail coif     | 10     | 32.8% |
| Ringmail tunic     | 18     | 35.2% |
| Chainmail leggings | 18     | 42.2% |
| Chainmail tunic    | 20     | 43.6% |
| Plate gorget       | 10     | 45.3% |
| Bascinet           | 15     | 47%   |
| Female breastplate | 25     | 47.3% |
| Gauntlets          | 12     | 47.7% |
| Open helm          | 15     | 49.2% |
| Armplates          | 15     | 49.7% |
| Legplates          | 20     | 52.1% |
| Closed helm        | 15     | 51.2% |
| Full helm          | 15     | 54.2% |
| Nose helm          | 15     | 55.2% |
| Breastplate        | 25     | 57%   |

### Shields

| Item               | Ingots | Skill |
| ------------------ | ------ | ----- |
| Buckler            | 6      | 17%   |
| Small round shield | 14     | 28.3% |
| Round shield       | 10     | 44.2  |
| Kite Shield        | 16     | 55.4% |
| Heater Shield      | 18     | 62%   |

### Fencing Weapons

| Item        | Ingots | Skill |
| ----------- | ------ | ----- |
| Short Spear | 3      | 12.2% |
| War Fork    | 5      | 17.2% |
| Dagger      | 3      | 17.7% |
| Pitchfork   | 6      | 22.3% |
| Spear       | 9      | 32.8% |
| Kyrss       | 7      | 37%   |
| Lajatang    | 16     | ?%    |
| Claws       | 7      | ?%    |

### Blade Weapons

| Item             | Ingots | Skill |
| ---------------- | ------ | ----- |
| Cutlass          | 12     | 32.2% |
| Axe              | 12     | 36.7% |
| Scimtar          | 13     | 39.3% |
| Battle axe       | 12     | 40.4% |
| Katana           | 9      | 42.1% |
| Two Handed axe   | 15     | 43.5% |
| Double axe       | 16     | 45%   |
| Long Sword       | 15     | 45.8% |
| Thin Sword       | 14     | 45.8% |
| Bardiche         | 14     | 47.3% |
| Broad sword      | 16     | 47.8% |
| Executioners axe | 16     | 57.8% |
| Great sword      | 17     | 62.2% |
| Halberd          | 18     | 62.4% |
| Large Battle axe | 17     | 62.8% |
| Crescent Blade   | 19     | ?%    |

### Blunt Weapons

| Item        | Ingots | Skill |
| ----------- | ------ | ----- |
| Hammer pick | 12     | 37.4% |
| War Hammer  | 14     | 42.7% |
| Mace        | 14     | 43.8% |
| Maul        | 16     | 50.5% |
| War Axe     | 17     | 57.4% |
| War Mace    | 18     | 59.8% |
