---
title: "Cooking"
date: 2018-10-30T22:17:46Z
draft: false
description: Cooking. You'll never attend the party, but everybody will love you
---

<!-- vim-markdown-toc GitLab -->

* [Related Skills](#related-skills)
* [Related Items](#related-items)
* [Notes](#notes)
* [Coming Soon](#coming-soon)
  * [Yin's Fortune Cookie](#yins-fortune-cookie)

<!-- vim-markdown-toc -->

As a Cook, you will be able to produce a variety of edible foods that characters
can consume which can, in some cases, induce magical affects.

Useful to keep your character well fed as it can affect the rate at which your
health regenerates.

Also useful for dinner parties.

## Related Skills

- [Fishing]({{< ref "fishing.md" >}})

## Related Items

- Flour Mill
- Mixing Bowl
- Wooden Bowl
- Cooking Pot

| Ingredient            | Skill | Requires                                                    | Instructions                                     | Effects                   |
| --------------------- | ----- | ----------------------------------------------------------- | ------------------------------------------------ | ------------------------- |
| Sack of Flour         | ?%   | Wheat sheaf                                                 | Double click wheat sheaf, target Flour Mill      |                           |
| Dough                 | 10%   | Sack of flour and water pitcher                             | Double click sack of flour, target water pitch   |                           |
| Sweet Dough           | 20%   | Dough and jar of honey                                      | Double click dough, target jar of honey          |                           |
| Cake Mix              | 20%   | Sweet Dough and sacks of flour                              | Double click sweet dough, target sack of flour   |                           |
| Cookie Mix            | 20%   | Sweet dough and jars of honey                               | Double click sweet dough, target jar of honey    |                           |
| Tomato soup           | 20%   | Tomatoes and water pitch                                    | Double click wooden bowl, target tomatoes & fire |                           |
| Stew                  | 30%   | Sacks of flour, raw ribs and water pitch                    | Double click wooden bowl, target raw ribs & fire |                           |
| Bread                 | 30%   | Dough                                                       | Double click dough, target oven                  |                           |
| Muffins               | 30%   | Sweet dough                                                 | Double click sweet dough, target oven            |                           |
| Unbaked Pumpkin Pie   | 30%   | Dough and pumpkins                                          | Double click dough, target pumpkins              |                           |
| Unbaked Fruit Pie     | 32%   | Dough and pears                                             | Double click dough, target pears                 |                           |
| Unbaked Peach Cobbler | 34%   | Dough and peaches                                           | Double click dough, target peaches               |                           |
| Unbaked Apple Pie     | 36%   | Dough and apples                                            | Double click dough, target apples                |                           |
| Unbaked Hunter's Pie  | 38%   | Dough and raw birds                                         | Double click dough, target raw birds             |                           |
| Unbaked Fish Pie      | 40%   | Dough and raw fish steaks                                   | Double click dough, target fish steak            |                           |
| Unbaked Trolls Pie    | 42%   | Dough and hams                                              | Double click dough, target hams                  |                           |
| Unbaked quiche        | 44%   | Dough and fresh eggs                                        | Double click dough, target fresh eggs            |                           |
| Cake                  | 45%   | Sweet dough                                                 | Double click sweet dough, target oven            |                           |
| Cookies               | 45%   | Cookie mix                                                  | Double click cookie mix, target oven             |                           |
| Uncooked Pizza        | 45%   | Dough, tomatoes and wheels of cheese                        | Double click dough, target tomatoes              |                           |
| Uncooked Ogre Pizza   | 55%   | Dough, tomatoes, hams, sausages, bacon and wheels of cheese | Double click dough, target sausages              |                           |
| Pumpkin Pie           | 40%   | Unbaked Pumpkin Pie                                         |                                                  |                           |
| Fruit Pie             | 42%   | Unbaked Fruit Pie                                           |                                                  |                           |
| Peach cobbler         | 44%   | Unbaked Peach Cobbler                                       |                                                  |                           |
| Apple Pie             | 46%   | Unbaked Apple Pie                                           |                                                  |                           |
| Hunter's Pie          | 48%   | Unbaked Hunter's Pie                                        |                                                  |                           |
| Fish Pie              | 50%   | Unbaked Fish Pie                                            |                                                  |                           |
| Trolls Pie            | 52%   | Unbaked Trolls Pie                                          |                                                  |                           |
| Quiche                | 54%   | Unbaked Quiche                                              |                                                  |                           |
| Pizza                 | 55%   | Uncooked Pizza                                              |                                                  |                           |
| Ogre Pizza            | 65%   | Uncooked Ogre Pizza                                         |                                                  |                           |
| White Chocolate       | 55%   |                                                             |                                                  | +10% Meditation           |
| Milk Chocolate        | 60%   |                                                             |                                                  | +5 dex                    |
| Dark Chocolate        | 65%   |                                                             |                                                  | +8 dex                    |
| Sushi                 | 80%   |                                                             |                                                  | +10 int, +5% Magery       |
| Shepherds Pie         | 95%   |                                                             |                                                  | +5% Taming                |
| Angel Pie             | 99%   |                                                             |                                                  | +20 str                   |
| Pie of Lords          | 100%  |                                                             |                                                  | +15 str, +15 dex, +15 int |

## Notes

- A mixing bowl and Cooking pots are available in Baker shops.
- Wooden bowls are available in Carpenters.
- Most items required to make foods are available in 4 kinds of NPCs:
  - Butchers
  - Farmers
  - Cooks
  - Bakers

## Coming Soon

### Yin's Fortune Cookie

Yin won the recent recipe competition with this recipe.

Ingredients:

- 1 flour
- 1 butter
- 1 sugar
- 2 milk
- 1 honey
- 1 blank scroll
- 1 scribe powder.

These cookies will tell you your fortune and can also give some random magic
effects.
