---
title: "Assessment"
date: 2018-11-01T22:53:54Z
draft: false
---

Otherwise known as Anatomy, Assesment allows one to gather information about an
opponent, its strongnesses and weaknesses. For that same reason, it adds bonuses in
combat by increasing the chance to score a critical hit. 

It can improve your attack damage by around 20%, as well as improve the amount
of health healed when using bandages.

To use the skill manually, select it from the skill menu then select a target.
The success of using the skill properly depends on your assessment skill. This
will display information about the player about their stats.

You can either buy lessons from NPCs, but you can also raise it to 75% via
sparring melee skills. After 75% you must manually use the skill to raise it.

When you have over 30%, this skill offers more information about steeds when use
in conjunction with Creature Lore

## Related Skills

- Creature Lore
- Healing

