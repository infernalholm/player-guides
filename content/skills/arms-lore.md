+++
title="Arms Lore"
date= 2018-10-30T20:13:42+00:00
weight=2
description= "Arms lore skill. Appraise armour and weapons."
draft= false
+++

<!-- vim-markdown-toc GitLab -->

* [Skill Guide](#skill-guide)
* [Related Skills](#related-skills)
* [Craft Guide](#craft-guide)

<!-- vim-markdown-toc -->

## Skill Guide

On active use (using the skill upon a weapon/armour), it yields specific
information on the item, including its damage/armour rating and condition, as
well as all sort of properties related to the item material and crafting
quality. It also marks the item as identified, if it isn't magical.

On passive use (clicking on an identified item), 30% Arms Lore allow you to read
the numeric information of the item properties (e.g.:, "30% Enhanced damage" as
opposed to "Enhanced damage"). This effect is also achieved with 30% Item Lore.

Arms Lore is also required for crafting weapons and armour with Blacksmithing,
ranged weapons with Bowery and armour with Tailoring. For more information,
visit the specific sections for these skills. You need 65% arms lore to be able
to craft from all metals.

The role of arms lore can become more noticeable when using the Blacksmith
skill.

## Related Skills

- [Blacksmithy]({{< ref "/skills/blacksmithy.md" >}})
- [Bowery]({{< ref "/skills/bowery.md" >}})
- [Tailoring]({{< ref "/skills/tailoring.md" >}})

## Craft Guide

| Metal      | Skill |
| ---------- | ----- |
| Bronze     | 30%   |
| Adamantine | 40%   |
| Titanium   | 55%   |
