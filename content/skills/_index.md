+++
title = "Skills"
date = 2018-11-01T14:44:30Z
weight = 1
chapter = true
+++

# Skills

## Trade Skills

- [Alchemy]({{< ref "alchemy.md" >}})
- [Blacksmithy]({{<ref "blacksmithy.md">}})
- [Bowery]({{< ref "bowery.md" >}})
- [Carpentry]({{< ref "carpentry.md" >}})
- [Cartography]({{< ref "cartography.md" >}})
- [Cooking]({{< ref "cooking.md" >}})
- Farming
- [Fishing]({{<ref "fishing.md" >}})
- [Inscription]({{<ref "inscription.md" >}})
- [Lumberjacking]({{<ref "lumberjacking.md">}})
- [Masonry]({{< ref "masonry.md" >}})
- [Mining]({{<ref "mining.md" >}})
- [Tailoring]({{< ref "tailoring.md" >}})
- [Taming]({{< ref "taming.md" >}})
- [Tinkering]({{<ref "tinkering.md">}})

## Combat Skills

- [Archery]({{<ref "archery.md" >}})
- [Assessment]({{<ref "assessment.md">}})
- Fencing
- Macefighting
- Magery
- Musicianship
- Resist Magic
- Shieldfighting
- Songs of War
- Swordsmanship
- Tactics
- Wrestling

## Lore Skills

- [Arms Lore]({{< ref "arms-lore.md" >}})
- Creature Lore
- Herbs Lore
- Item Lore
- Mystic Lore
- Netherworld Lore

## Other Skills

- Detection
- Hiding
- Meditation
- Necromancy (coming soon)
- Stealth


