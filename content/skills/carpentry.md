+++
title="Carpentry"
date = 2018-10-30T20:13:42+00:00
description = "Carpentry. Used to create shields, wooden weapons and decorations"
+++

<!-- vim-markdown-toc GitLab -->

* [Skill Guide](#skill-guide)
* [Related Skills](#related-skills)
* [Related Items](#related-items)
* [Furniture](#furniture)
* [Assembled Items](#assembled-items)
* [Weapons and Shields](#weapons-and-shields)
* [Training Weapons](#training-weapons)
* [Miscellaneous Items](#miscellaneous-items)

<!-- vim-markdown-toc -->

## Skill Guide

Carpentry is a traditional decorational skill, which allows artisans to make a
wide selection of furniture, wooden shields, staves, along with other misc items
which would be useful for other traders, for example scrolls, and dying tubs.

Most of the general items you can craft, require a combination of logs, iron
nails and hinges. Other items may require parts crafted using other skills.
Some examples of this are: anvils, forges, spinning wheels, and small ships.


[Tinkering({{<ref "tinkering.md">}}) is necessary to make anvil, forge and spinning wheel parts.
[Tinkering({{<ref "tinkering.md">}}) is also necessary to make tailored small ship parts.
Tailoring and a small amount of tinkering is necessary to make Tailored small
ship parts.

If you want to make more than one scroll are a time, select the logs using a
dagger, with a saw in your pack to attempt to craft more than one.

Myst and Imbued furniture all require 100% carpentry to make.
Interleaved furniture requires the same skill amount as Oak furniture
Composite furniture requires the same skill amount as Cherry furniture.
Imbued weapons/shields require high arms lore to craft as well as carpentry

## Related Skills

- [Lumberjacking]({{<ref "lumberjacking.md">}})
- [Arms Lore]({{< ref "arms-lore.md" >}})
- [Tinkering({{<ref "tinkering.md">}})
- [Tailoring]({{< ref "tailoring.md" >}})

## Related Items

- Saw
- Nails
- Logs

## Furniture

| Item                   | Resources Needed                           | Skill Required          |
| ---------------------- | ------------------------------------------ | ----------------------- |
| Pine Stool             | 9 Logs, 1 Iron Nails                       | 11.0%                   |
| Pine Stool w.Bar       | 9 Logs, 1 Iron Nails                       | 13.0%                   |
| Rough Pine Chair       | 10 logs, 1 Iron Nails                      | 14.0%                   |
| Pine Chair             | 13 Logs, 1 Iron Nails                      | 18.0%                   |
| Pine W.Seat Chair      | 13 Logs, 1 Iron Nails                      | 24.0%                   |
| Pine Chair w.cushion   | 13 Logs, 1 Iron Nails, 5 cloth             | 33.0% (15.0% tailoring) |
| Pine Wooden Bench      | 10 Logs, 1 Iron Nails                      | 40.0%                   |
| Pine Night Stand       | 14 Logs, 3 Iron Nails                      | 42.1%                   |
| Smooth Pine Table      | 10 Logs, 3 Iron Nails                      | 50.1%                   |
| Pine Bookshelf         | 12 Logs, 6 Iron Nails, 5 small book (blue) | 55.5%                   |
| Pine Drawers           | 17 Logs, 3 Iron Nails, 3 Iron Hinges       | 56.5%                   |
| Pine Writing Table     | 17 Logs, 3 Iron Nails                      | 59.1%                   |
| Rough Pine Table       | 23 Logs, 1 Iron Nails                      | 63.1%                   |
| Pine Throne            | 17 Logs, 1 Iron Nails                      | 62.6%                   |
| Pine Dresser           | 20 Logs, 5 Iron Nails, 4 Iron Hinges       | 65.5%                   |
| Pine Armoire           | 23 Logs, 6 Iron Nails, 4 Iron Hinges       | 74.2%                   |
| Large Rough Pine Table | 35 Logs, 3 Iron Nails                      | 78%                     |

## Assembled Items

| Item                       | Resources Needed                                     | Skill Required      |
| -------------------------- | ---------------------------------------------------- | ------------------- |
| Deed to an Anvil           | 1 Anvil Parts, 20 Pine Log                           | 75.0%               |
| Deed to a Forge            | 1 Forge Parts, 30 Pine Log                           | 70.0%               |
| Deed to a Spinning Wheel   | 1 Spinning Wheel Parts, 30 Pine Log, 2 Iron Nails    | 73.6%               |
| Small Ship Deed            | 5 Carpentry, 5 Tailored, 5 Tinkered Small Ship Parts | 90.0%               |
| Small Ship Carpentry Parts | 200 Pine Log                                         | 70% (30% tinkering) |

## Weapons and Shields

| Items                | Resources Needed      | Skill Required |
| -------------------- | --------------------- | -------------- |
| Pine Shepard's Crook | 4 Logs                | 20.0%          |
| Pine Quarterstaff    | 3 Logs                | 25.0%          |
| Pine Club            | 4 Logs                | 30.0%          |
| Pine Gnarled Staff   | 4 Logs                | 47.9%          |
| Pine Fine Staff      | 8 Logs                | 75.0%          |
| Pine Round Shield    | 7 Logs, 1 Iron Nails  | 22.6%          |
| Pine Kite Shield     | 10 Logs, 1 Iron Nails | 52.6%          |

## Training Weapons

| Items             | Resources Needed  | Skill Required |
| ----------------- | ----------------- | -------------- |
| Wooden Thin Sword | 9 eucalyptus logs | 40.0%          |
| Wooden Mace       | 9 eucalyptus logs | 40.0%          |
| Wooden Kryss      | 9 eucalyptus logs | 40.0%          |

## Miscellaneous Items

| Items                      | Resources Needed                                                              | Skill Required          |
| -------------------------- | ----------------------------------------------------------------------------- | ----------------------- |
| Dying Tub                  | 5 Pine Logs                                                                   | 50.0%                   |
| Backpack Dye Tub           | 5 Pine Logs, 5 Iron Nails                                                     | 80.0%                   |
| Hair Dye Tub               | 5 Pine Logs, 5 Iron Nails                                                     | 85.0%                   |
| Rare-Dye Multi Storage Tub | 10 Pine Logs, 5 Eucalyptus Logs, 5 Holly Logs, 5 Rose Logs and 5 Iron Nails   | 75.0%                   |
| Iron Nail Barrel Tub       | 12 Pine Logs, 2 Barrel Hoops, 36 Iron Nails, 1 Iron Hinge                     | 66.0% (67.0% Tinkering) |
| Empty Keg                  | 12 Pine Logs, 1 Barrel Tap, 2 Barrel Hoops, 36 Iron Nails, 1 Beeswax, 1 Hinge |                         |
| Blank Scroll               | 1 Pine Log                                                                    |                         |
| Wooden Sword               | 9 Eucalyptus logs                                                             | 41.7%                   |
