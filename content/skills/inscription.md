---
title: 'Inscription'
date: 2018-11-01T18:28:21Z
draft: false
---

<!-- vim-markdown-toc GitLab -->

* [Related Skills](#related-skills)
* [Related Items](#related-items)
* [Scrolls](#scrolls)
* [Other Items](#other-items)
* [Steed Books](#steed-books)

<!-- vim-markdown-toc -->

Copy scrolls, books, and inscribe runic characters in magical objects.

## Related Skills

- Magery

## Related Items

- Pen and Ink
- Scribe Powder
- Blank Scrolls
- Magery Reagents

## Scrolls

The crafting of a scroll requires the same reagents that the spell requires, and
half the mana.

| Scroll                 | Circle | Inscription % | Magery % |
| ---------------------- | ------ | ------------- | -------- |
| Reactive Armor         | 1      | 12%           | 10%      |
| Clumsy                 | 1      | 10%           | 10%      |
| Feeblemind             | 1      | 10%           | 10%      |
| Heal                   | 1      | 9%            | 10%      |
| Magic Arrow            | 1      | 7%            | 10%      |
| Night Sight            | 1      | 10%           | 10%      |
| Weaken                 | 1      | 10%           | 10%      |
| Agility                | 2      | 25%           | 20%      |
| Cunning                | 2      | 20%           | 20%      |
| Cure                   | 2      | 20%           | 20%      |
| Harm                   | 2      | 20%           | 20%      |
| Magic Trap             | 2      | 20%           | 20%      |
| Magic Untrap           | 2      | 20%           | 20%      |
| Protection             | 2      | 25%           | 20%      |
| Strength               | 2      | 25%           | 20%      |
| Bless                  | 3      | 35%           | 30%      |
| Fireball               | 3      | 30%           | 30%      |
| Magic Lock             | 3      | 30%           | 30%      |
| Poison                 | 3      | 35%           | 30%      |
| Telekenisis            | 3      | 30%           | 30%      |
| Teleport               | 3      | 30%           | 30%      |
| Magic Unlock           | 3      | 30%           | 30%      |
| Wall of Stone          | 3      | 35%           | 30%      |
| Arch Cure              | 4      | 40%           | 40%      |
| Arch Protection        | 4      | 40%           | 40%      |
| Curse                  | 4      | 40%           | 40%      |
| Fire Field             | 4      | 50%           | 40%      |
| Greater Heal           | 4      | 45%           | 40%      |
| Lightning              | 4      | 52%           | 40%      |
| Mana Drain             | 4      | 40%           | 40%      |
| Recall                 | 4      | 55%           | 40%      |
| Blade Spirits          | 5      | 60%           | 50%      |
| Dispel Field           | 5      | 55%           | 50%      |
| Incognito              | 5      | 60%           | 50%      |
| Magic Reflection       | 5      | 65%           | 50%      |
| Mind Blast             | 5      | 55%           | 50%      |
| Paralyze               | 5      | 65%           | 50%      |
| Poison Field           | 5      | 55%           | 50%      |
| Summon Creature        | 5      | 65%           | 50%      |
| Dispel                 | 6      | 70%           | 60%      |
| Energy Bolt            | 6      | 75%           | 60%      |
| Explosion              | 6      | 70%           | 60%      |
| Invisibility           | 6      | 75%           | 60%      |
| Mark                   | 6      | 80%           | 60%      |
| Mass Curse             | 6      | 70%           | 60%      |
| Paralyze Field         | 6      | 75%           | 60%      |
| Reveal                 | 6      | 70%           | 60%      |
| Chain Lightning        | 7      | 85%           | 70%      |
| Energy Field           | 7      | 90%           | 70%      |
| Flamestrike            | 7      | 95%           | 70%      |
| Gate Travel            | 7      | 95%           | 70%      |
| Mana Vampire           | 7      | 90%           | 70%      |
| Mass Dispel            | 7      | 95%           | 70%      |
| Meteor Swarm           | 7      | 90%           | 70%      |
| Polymorph              | 7      | 90%           | 80%      |
| Earthquake             | 8      | 90%           | 70%      |
| Energy Vortex          | 8      | 98%           | 80%      |
| Resurrection           | 8      | 99%           | 80%      |
| Summon Air Elemental   | 8      | 95%           | 80%      |
| Summon Daemon          | 8      | 99%           | 80%      |
| Summon Earth Elemental | 8      | 95%           | 80%      |
| Summon Fire Elemental  | 8      | 95%           | 80%      |
| Summon Water Elemental | 8      | 95%           | 80%      |

## Other Items

| Item/Task                        | Resources                                                                               | Skill Required |
| -------------------------------- | --------------------------------------------------------------------------------------- | -------------- |
| Copy a book                      | 20 blank scrolls, 1 spool of thread, 1 cut leather, 1 book                              | 65%            |
| Finish a book                    | scribe powder                                                                           | 65%            |
| Rent a shelf on a library        | -                                                                                       | 75%            |
| Small book of runes (30 charges) | 10 recall scrolls, 10 gate scrolls, 1 spool of thread, 1 leather hide                   | 98%            |
| Book of runes (100 charges)      | 30 recall scrolls, 30 gate scrolls, 30 blank scrolls, 1 spool of thread, 1 leather hide | 100%           |
| Runebook charger (20 charges)    | 10 blank scrolls, 5 mark scrolls                                                        | 100%           |
| Commodity deed                   | 15 madrake root, 15 garlic, 5 scribe powder, 1 blank scroll                             | 100%           |
| Address book                     | 1 cut leather, 1 spool of thread, 10 blank scrolls, 1 folded cloth                      | ?%             |
| Origami                          | ?                                                                                       | ?%             |

{{% notice tip %}}
Rune Chargers are an alternative to using the mark scroll to charge rune books. 
{{% /notice %}}

{{% notice warning %}}
Neither Runebooks, nor necromancy books are 'newbied'. If you lose your life
in-game, the books will stay in your corpse, available for other players to
loot.
{{% /notice %}}

## Steed Books

Steed books store shrunk steeds of the same type.

| Item Name  | Resources                                          | Inscription> | Creature Lore |
|------------|----------------------------------------------------|--------------|---------------|
| Steed Book | 1 cut leather, 1 spool of thread, 10 blank scrolls | 80%          | 50%           |

{{% notice tip %}}
When making Steed Books, once you have the required resources and skill, target
the desired steed with a Pen and Ink set to make the book. 
{{% /notice %}}


