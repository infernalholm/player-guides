+++
title="Alchemy"
date= 2018-10-30T20:13:42+00:00
weight=1
description= "Alchemy skill. Used to create potions and dyes."
draft= false
+++

<!-- vim-markdown-toc GitLab -->

* [Related Skills](#related-skills)
* [Related Items](#related-items)
* [Alchemy Dyes](#alchemy-dyes)
* [How to raise](#how-to-raise)

<!-- vim-markdown-toc -->


## Related Skills

- [Tailoring]({{< ref "tailoring.md" >}})
- [Poisoning](Poisoning)

## Related Items

- Mortar and Pestle
- Reagents
- Empty Bottle
- Dye Tub

## Alchemy Dyes

| Dye                  | Resources                                                                 | Skill Required | Color Code |
| -------------------- | ------------------------------------------------------------------------- | -------------- | ---------- |
| Soft Purple          | 20 Blood Moss, 20 Black Pearl, 20 Sulfurous Ash                           | 55%            | 048b       |
| Green                | 20 Bone, 20 Dragon Blood, 20 Serpents Scales                              | 60%            | 0487       |
| Soft Green           | 20 Bone, 20 Dragon Blood, 20 Serpents Scales                              | 60%            | 048a       |
| Pitch Black          | 20 Obsidian, 20 Batwing, 20 Pig Iron                                      | 63%            | 0485       |
| Soft Blue            | 20 Obsidian, 20 Bone, 20 Eye of Newt                                      | 64%            | 048d       |
| Blue                 | 20 Obsidian, 20 Bone, 20 Eye of Newt                                      | 66%            | 0489       |
| Passion Purple       | 20 wryms hearts, 20 pumice, 20 black pearls                               | 67%            | 0ac9       |
| Bronze               | 20 Blood Spawn, 20 Blackmoor, 20 Fertile Dirt                             | 69%            | 0488       |
| Purple               | 20 Brimstone, 20 Executioners Cap, 20 Blood Vials                         | 72%            | 0483       |
| Pure White           | 20 Bone, 20 Pumice, 20 Volcanic Ash                                       | 75%            | 0484       |
| Strong Pink          | 20 Brimstone, 20 Blood Vials, 20 Dragon Blood                             | 75%            | 0a10       |
| Swamp                | 20 nightshade, 20 dragon blood, 20 fertile dirt                           | 76%            | 09f6       |
| White                | 20 Bone, 20 Pumice, 20 Wyrms Heart                                        | 77%            | 0481       |
| Passion Pink         | 20 Bone, 20 Pumice, 20 Wyrms Heart                                        | 78%            | 09b1       |
| Fog                  | 20 pumice, 20 spider silk, 20mongbat jaw, 20 dragon blood                 | 82%            | 07eb       |
| Dark Bronze          | 20 Bloodspawn, 30 Blackmoor, 25 Fertile Dirt                              | 85%            | 079e       |
| Angel                | 20 Obsidian, 20 Pumice, 20 Wyrms Heart                                    | 85%            | 048f       |
| Fire                 | 20 Brimstone, 20 Dragon Blood, 20 Blood Vials                             | 85%            | 0486       |
| Cardinal             | 20 blood vials, 20 brimstone, 20 blood moss                               | 87%            | 0af1       |
| Ice Blue             | 20 Obsidian, 20 Pumice, 20 Wyrms Heart                                    | 90%            | 0480       |
| Black                | 20 Obsidian, 20 Batwing, 20 Black Pearl, 20 Pig Iron                      | 95%            | 0455       |
| Rain                 | 20 black pearls, 20 spider silk, 20 obsidian, 20 Darkwood                 | 97%            |            |
| Sunset               | 20 sulfurous ash, 20 blood moss, 20 blood vials, 20 brimstone             | 99%            |            |
| Mellow Yellow        | 20 Sulfurous Ash , 20 Volcanic Ash,20 Nightshade                          | ?%             |            |
| Forest               | 20 Nightshade, 20 Garlic, 20 Bone                                         | ?%             |            |
| Peppermint           | 20 black pearl,20 pumice,20 wyrm's hearth                                 | ?%             |            |
| Silken Purple        | 20 Black Pearl, 20 Brimstone, 20 Mandrake Root                            | ?%             |            |
| Cerulean             | 20 Black Pearl, 20 Spider's Silk, 20 Wyrm's Heart, 20 Mongbat Jaw         | ?%             |            |
| Dread                | 20 Mandrake Root, 20 Brimstone, 20 Blood Moss                             | ?%             |            |
| Deep Sea Blue        | 20 Obsidian, 20 Nightshade, 20 Wyrm's Heart, 10 Dragons Heart             | ?%             |            |
| Phantom              | 20 Blood Vial, 20 Garlic, 20 Wyrm's Heart                                 | ?%             |            |
| Spectre              | 1 white, 20 dark wood, 20 Executioner's Cap, 20 Obsidian                  | ?%             |            |
| Electric Blue        | 1 soft blue, 20 nox cristal, 20 dragon blood, 20 wyrms heart              | ?%             |            |
| Purple Rain          | ?                                                                         | ?%             |            |
| Frozen Blue          | 1 blue, 1 ice blue, 20 wyrms hear, 20 orc spine                           | ?%             |            |
| Citrus               | 1 fire , 20 blood moss 20 sulf ash (or dragon blood ?), 20 blood vials    | ?%             |            |
| Classic Passion Pink | 20 bone, 20 Pumice, 20 Wyrm's Heart, 10 Brimstone, 20 Sulfurous Ash       | 105%           |            |
| Classic Strong Pink  | 20 Blood Vial, 20 Brimstone, 20 Dragons Blood, 20 Wyrm's Heart            | 105%           |            |
| Classic Fire         | 1 fire, 20 sulf ash, 20 blood moss, 20 brimstone                          | 105%           |            |
| Heavenly White       | 1 angel, 1 white, 1 mytheril ingot, 2 angel dust, 50 ginseng, 50 obsidian | 110%           |            |
| Vorpal               | ?                                                                         | 115%           |            |


## How to raise

*Macro*: `F1` – Last Object (Mortar and Pester)

```
PRESS KEY F1
Delay 10000
```

Suggested Potions:

| Skill interval | Potion            | Sell to              |
| -------------- | ----------------- | ---------------------|
| 50% - 52%      | Lesser Strength   | Alchemy Vendor       |
| 52% - 55%      | Lesser Cleverness | Alchemy Vendor       |
| 55% - 58%      | Refresh           | Alchemy Vendor       |
| 58% - 61%      | Cure Poison       | Alchemy Vendor       |
| 61% - 66%      | Agility           | Alchemy Vendor       |
| 66% - 69%      | Strength          | Alchemy Vendor       |
| 69% - 71%      | Cleverness        | Alchemy Vendor       |
| 71% - 74%      | Permanence        | Alchemy Vendor       |
| 74% - 79%      | Greater Refresh   | Reward board tickets |
| 79% - 80%      | Greater Strength  | Reward board tickets |
| 80% - 100%     | Nightsight        | Alchemy Vendor       |

*Credits*: Wigifer for providing most of this information.
