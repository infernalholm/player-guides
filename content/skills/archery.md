---
title: 'Archery'
date: 2018-11-01T22:26:12Z
draft: false
---

<!-- vim-markdown-toc GitLab -->

* [Related Skills](#related-skills)
* [Related Items](#related-items)
* [Bows](#bows)
* [Crossbows](#crossbows)
* [Heavy Crossbows](#heavy-crossbows)

<!-- vim-markdown-toc -->

The archery skill allows players to equip better bows.

You need to stay still and at least one tile away from your target to shoot it.

When swapping between bow and crossbow you have to double click on the new
arrows/bolts that you wish to use with that bow type. This equips them as your
'current ammo'.

## Related Skills

- [Bowery]({{<ref "bowery.md">}})
- [Assessment]({{<ref "assessment.md">}})

## Related Items

- Arrows
- Crossbow Bolts

## Bows

| Bow         | Skill |
| ----------- | ----- |
| Pine        | 0.1%  |
| Eucalyptus  | 34.8% |
| Holly       | 39.8% |
| Rose        | 39.8% |
| Oak         | 44.8% |
| Interleaved | 59.8% |
| Ironwood    | 59.8% |
| Mahogany    | 64.8% |
| Cherry      | 64.8% |
| Composite   | 85%   |
| Myst        | 85%   |

## Crossbows

| Crossbow    | Skill |
| ----------- | ----- |
| Pine        | 29.8% |
| Eucalyptus  | 51.8% |
| Holly       | 56.8% |
| Rose        | 56.8% |
| Oak         | 61.8% |
| Interleaved | 71.8% |
| Ironwood    | 76.8% |
| Mahogany    | 81.8% |
| Cherry      | 81.8% |
| Composite   | 90%   |
| Myst        | 90%   |

## Heavy Crossbows

| Heavy Crossbow | Skill |
| -------------- | ----- |
| Pine           | 54.4% |
| Eucalyptus     | 59.4% |
| Holly          | 64.4% |
| Rose           | 64.4% |
| Oak            | 65.0% |
| Interleaved    | 79.4% |
| Ironwood       | 85.0% |
| Mahogany       | 89.4% |
| Cherry         | 89.4% |
| Composite      | 95%   |
| Myst           | 95%   |
