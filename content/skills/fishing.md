+++
title= "Fishing"
date= 2018-10-30T21:31:28Z
draft= false
description= "Fishing. Where fishes have names from terrestrial animals."
+++

<!-- vim-markdown-toc GitLab -->

- [Related Skills](#related-skills)
- [Related Items](#related-items)
- [Fishes](#fishes)

<!-- vim-markdown-toc -->

This skill allows players to gather fish from most areas with water or rivers.

When you catch generic fish, you can cut them up with a knife and sell their
steaks for 3gp each. Each fish gives 4 steaks so each fish can render 12gp.

Shipwrights and some players sell ships.

## Related Skills

- [Cooking]({{<ref "cooking.md">}})
- [Cartography]({{< ref "cartography.md" >}})

## Related Items

- Fishing Pole
- Boat

## Fishes

| Fish                  | Skill  | Effect                                |
| --------------------- | ------ | ------------------------------------- |
| Highly Peculiar Fish  | 65.0%  | Restores stamina at a higher speed    |
| Camel Fish            | 69.9%  | Restores hit points at a higher speed |
| Owl Fish              | 79.9%  | Restores Mana at a higher speed       |
| Prize Fish            | 75.0%  | Cunning spell                         |
| Wondrous Fish         | 85.0%  | Agility spell                         |
| Truly Rare Fish       | 100.0% | Strength spell                        |
| Sage Fish             | 89.9%  | Random INT bonus                      |
| Lion Fish             | 89.9%  | Random STR bonus                      |
| Monkey Fish           | 89.9%  | Random DEX bonus                      |
| Angel Fish            | 89.9%  | Random Heal bonus                     |
| Random Fishing junk   | 40.0%  |                                       |
| Sodden Flesh          | 75.0%  |                                       |
| Tattered Treasure Map | 90.0%  |                                       |
| Holy Fish             | 99.9%  | Cleans a "kill" from your papers      |
| Water Elemental       | ?%     | You lose your life                    |
