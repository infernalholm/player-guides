+++
title="Bowery"
date = "2018-10-30T20:13:42+00:00"
description = "Bowery. Create Bows, bolts and wood based alloys"
+++

<!-- vim-markdown-toc GitLab -->

* [Skill Guide](#skill-guide)
* [Related Skills](#related-skills)
* [Related Items](#related-items)
* [Craftables](#craftables)
* [Alloy Wood](#alloy-wood)

<!-- vim-markdown-toc -->

## Skill Guide

The bowcraft skill allows one to create bows, crossbows and mixed wood types
of high quality.

You may purchases Bowery lessons from Bowyer NPC's found in most towns. 
You will require [Lumberjacking]({{<ref "lumberjacking.md">}}) to gather logs,
and [Tinkeringa]({{<ref "tinkering.md">}}) for making hinges and wire for the
better bows. To begin crafting, equip a Dagger and target a pile of logs with
it. 
If you have the correct items a menu will prompt.

## Related Skills

| Skill                                         | Description                                                                           |
|-----------------------------------------------|---------------------------------------------------------------------------------------|
| [Arms Lore]({{< ref "arms-lore.md" >}})       | Required to craft special wood types                                                  |
| [Lumberjacking]({{<ref "lumberjacking.md">}}) | Required to harvest wood                                                              |
| [Carpentry]({{< ref "carpentry.md" >}})       | Used to craft items made with alloys created with bowery                              |
| Tinkering                                     | Required for the making of hinges and wire used in making bows, and also for jeweling |
| [Alchemy]({{< ref "alchemy.md" >}})           | Required to make Imbued Cherry                                                        |

## Related Items

| Item   | Description                                                                                                  |
| ------ | ------------------------------------------------------------------------------------------------------------ |
| Dagger | Using a dagger on logs, while carrying the correct resources, will allow you to craft items with this skill. |

## Craftables

| Item                       | Resources                                               | Bowery |
| -------------------------- | ------------------------------------------------------- | ------ |
| pine bow                   | 10 logs, 1 cut hide                                     | 29.8%  |
| eucalyptus bow             | 10 logs, 1 cut hide                                     | 34.8%  |
| holly bow                  | 10 logs, 1 cut hide                                     | 39.8%  |
| rose bow                   | 10 logs, 1 cut hide                                     | 39.8%  |
| oak bow                    | 10 logs, 1 cut hide                                     | 44.8%  |
| pine crossbow              | 10 logs, 1 iron wire                                    | 46.8%  |
| eucalyptus crossbow        | 10 logs, 1 iron wire                                    | 51.8%  |
| pine heavy crossbow        | 12 logs, 1 iron hinge, 1 steel wire                     | 54.4%  |
| interleaved bow            | 10 logs, 1 cut hide                                     | 54.8%  |
| holly crossbow             | 10 logs, 1 iron wire                                    | 56.8%  |
| rose crossbow              | 10 logs, 1 iron wire                                    | 56.8%  |
| eucalyptus heavy crossbow  | 12 logs, 1 iron hinge, 1 steel wire                     | 59.4%  |
| ironwood bow               | 10 logs, 1 cut hide                                     | 59.8%  |
| oak crossbow               | 10 logs, 1 iron wire                                    | 61.8%  |
| holly heavy crossbow       | 12 logs, 1 iron hinge, 1 steel wire                     | 64.4%  |
| rose heavy crossbow        | 12 logs, 1 iron hinge, 1 steel wire                     | 64.4%  |
| mahogany bow               | 10 logs, 1 cut hide                                     | 64.8%  |
| cherry bow                 | 10 logs, 1 cut hide                                     | 64.8%  |
| oak heavy crossbow         | 12 logs, 1 iron hinge, 1 steel wire                     | 69.4%  |
| composite bow              | 10 logs, 1 cut hide                                     | 69.8%  |
| interleaved crossbow       | 10 logs, 1 iron wire                                    | 71.8%  |
| ironwood crossbow          | 10 logs, 1 iron wire                                    | 76.8%  |
| interleaved heavy crossbow | 12 logs, 1 iron hinge, 1 steel wire                     | 79.4%  |
| mahogany crossbow          | 10 logs, 1 iron wire                                    | 81.8%  |
| cherry crossbow            | 10 logs, 1 iron wire                                    | 81.8%  |
| ironwood heavy crossbow    | 12 logs, 1 iron hinge, 1 steel wire                     | 84.4%  |
| composite crossbow         | 10 logs, 1 iron wire                                    | 86.8%  |
| mahogany heavy crossbow    | 12 logs, 1 iron hinge, 1 steel wire                     | 89.4%  |
| cherry heavy crossbow      | 12 logs, 1 iron hinge, 1 steel wire                     | 89.4%  |
| composite heavy crossbow   | 12 logs, 1 iron hinge, 1 steel wire                     | 94.4%  |
| myst bow                   | 10 logs, 1 cut hide                                     | ?%     |
| myst crossbow              | 10 logs, 1 iron wire                                    | ?%     |
| myst heavy crossbow        | 12 logs, 1 iron hinge, 1 steel wire                     | ?%     |
| imbued cherry crossbow     | 10 logs,1 stronghold wire                               | 100%   |
| dragon heavy crossbow      | dragon xbow mechanism, dragon tooth, imbued cherry logs | ?%     |

## Alloy Wood

| Alloy         | Resources                                                                    | Bowery | Arms Lore | Observations        |
| ------------- | ---------------------------------------------------------------------------- | ------ | --------- | ------------------- |
| Interleaved   | 6 eucalyptus logs, 3 rose logs                                               | 60%    | 30%       |                     |
| composite     | 6 mahogany logs, 4 ironwood logs, 2 cherry logs                              | 100%   | 40%       |                     |
| Imbued Cherry | 1 cherry log, 10 dragon blood, 10 daemon bones, 10 Ectoplasm, 1 empty Bottle | 100%   | 50%       | Has special effects |
