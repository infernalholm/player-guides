---
title: 'Tinkering'
date: 2018-11-01T20:45:13Z
draft: false
---

Tinkering is a skill which allows for the crafting of utilities and
decorative items out of wood and metals.

I also used to craft jewels which can improve weapons and armours.

At high levels, artisans can also use tinkering to make and animate Golems.

## Related Skills

- [Arms Lore]({{<ref "arms-lore.md">}})
- [Item Lore]({{<ref "">}})
- [Blacksmithy]({{<ref "blacksmithy.md">}})
- [Carpentry]({{<ref "carpentry.md">}})
- [Masonry]({{<ref "masonry.md">}})

## Related Items

- Tinker Tools
- Ingots
- Gems

## Craftables

### Metal Parts

| Item                | Resources           | Skill |
| ------------------- | ------------------- | ----- |
| Copper Nails        | 1 Copper Ingot      | 2.0%  |
| Pure Copper Nails   | 1 Pure Copper Ingot | 3.0%  |
| Iron Nails          | 1 Iron Ingot        | 10.0% |
| Ferrum Nails        | 1 Ferrum Ingot      | 14.0% |
| Bronze Nails        | 1 Bronze Ingot      | 15.0% |
| Brass Nails         | 1 Brass Ingot       | 16.0% |
| Steel Nails         | 1 Steel Ingot       | 17.5% |
| Copper Gears        | 1 Copper ingot      | 17.7% |
| Chrome Nails        | 1 Chrome Ingot      | 18.5% |
| Pure Copper Gears   | 1 Pure Copper Ingot | 18.7% |
| Silver Nails        | 1 Silver Ingot      | 19.5% |
| Iron Gears          | 1 Iron Ingot        | 25.7% |
| Ferrum Gears        | 1 Ferrum Ingot      | 29.7% |
| Bronze Gears        | 1 Bronze Ingot      | 30.7% |
| Copper Springs      | 1 Copper ingot      | 30.7% |
| Pure Copper Springs | 1 Pure Copper Ingot | 31.7% |
| Brass Gears         | 1 Brass Ingot       | 31.7% |
| Copper Wire         | 1 Copper ingot      | 32.0% |
| Pure Copper Wire    | 1 Pure Copper Ingot | 33.0% |
| Steel Gears         | 1 Steel Ingot       | 33.2% |
| Chrome Gears        | 1 Chrome Ingot      | 34.2% |
| Silver Gears        | 1 Silver Ingot      | 35.2% |
| Iron Springs        | 1 Iron Ingot        | 38.7% |
| Iron Wire           | 1 Iron Ingot        | 40.0% |
| Ferrum Springs      | 1 Ferrum Ingot      | 42.7% |
| Bronze Springs      | 1 Bronze Ingot      | 43.7% |
| Ferrum Wire         | 1 Ferrum Ingot      | 44.0% |
| Brass Springs       | 1 Brass Ingot       | 44.7% |
| Bronze Wire         | 1 Bronze Ingot      | 45.0% |
| Brass Wire          | 1 Brass Ingot       | 46.0% |
| Steel Springs       | 1 Steel Ingot       | 46.2% |
| Chrome Springs      | 1 Chrome Ingot      | 47.2% |
| Steel Wire Steel    | 1 Ingot             | 47.5% |
| Silver Springs      | 1 Silver Ingot      | 48.2% |
| Chrome Wire         | 1 Chrome Ingot      | 48.5% |
| Silver Wire         | 1 Silver Ingot      | 49.5% |
| Copper Hinge        | 1 Copper ingot      | 42.8% |
| Pure Copper Hinge   | 1 Pure Copper Ingot | 43.8% |
| Iron Hinge          | 1 Iron Ingot        | 50.8% |
| Titanium Nails      | 1 Titanium Ingot    | 52.5% |
| Ferrum Hinge        | 1 Ferrum Ingot      | 54.8% |
| Adamantine Nails    | 1 Adamantine Ingot  | 55.0% |
| ronze Hinge         | 1 Bronze Ingot      | 55.8% |
| Brass Hinge         | 1 Brass Ingot       | 56.8% |
| Terathane Nails     | 1 Terathane Ingot   | 57.5% |
| Steel Hinge Steel   | 1 Ingot             | 58.3% |
| Chrome Hinge        | 1 Chrome Ingot      | 59.3% |
| Phoenix Nails       | 1 Phoenix Ingot     | 60.0% |
| Silver Hinge        | 1 Silver Ingot      | 60.3% |
| Titanium Gears      | 1 Titanium Ingot    | 60.3% |
| Adamantine Gears    | 1 Adamantine Ingot  | 62.8% |
| Terathane Gears     | 1 Terathane Ingot   | 65.3% |
| Titanium Springs    | 1 Titanium Ingot    | 66.8% |
| Titanium Wire       | 1 Titanium Ingot    | 67.5% |
| Phoenix Gears       | 1 Phoenix Ingot     | 67.8% |
| Adamantine Springs  | 1 Adamantine Ingot  | 69.3% |
| Adamantine Wire     | 1 Adamantine Ingot  | 70.0% |
| Terathane Springs   | 1 Terathane Ingot   | 71.8% |
| Terathane Wire      | 1 Terathane Ingot   | 72.5% |
| Titanium Hinge      | 1 Titanium Ingot    | 72.9% |
| Phoenix Springs     | 1 Phoenix Ingot     | 74.3% |
| Phoenix Wire        | 1 Phoenix Ingot     | 75.0% |
| Adamantine Hinge    | 1 Adamantine Ingot  | 75.4% |
| Terathane Hinge     | 1 Terathane Ingot   | 77.9% |
| Phoenix Hinge       | 1 Phoenix Ingot     | 80.4% |

### Decorations

| Item                      | Resources                | Skill |
| ------------------------- | ------------------------ | ----- |
| Small Copper Brazier      | Copper ingots, Coal      | 47.1% |
| Small Pure Copper Brazier | Pure Copper Ingots,Coal  | 48.1% |
| Small Iron Brazier        | Iron Ingots, Coal        | 55.1% |
| Pure Copper Brazier       | Pure Copper Ingots, Coal | 58.1% |
| Small Ferrum Brazier      | Ferrum Ingots, Coal      | 59.1% |
| Small Bronze Brazier      | Bronze Ingots, Coal      | 60.1% |
| Small Brass Brazier       | Brass Ingots, Coal       | 61.1% |
| Small Steel Brazier       | Steel Ingots, Coal       | 63.6% |
| Small Silver Brazier      | Silver Ingots, Coal      | 64.6% |
| Iron Brazier              | Iron Ingots, Coal        | 65.1% |
| Ferrum Brazier            | Ferrum Ingots, Coal      | 69.1% |
| Bronze Brazier            | Bronze Ingots, Coal      | 70.1% |
| Brass Brazier             | Brass Ingots, Coal       | 71.1% |
| Steel Brazier             | Steel Ingots, Coal       | 72.6% |
| Chrome Brazier            | Chrome Ingots, Coal      | 73.6% |
| Silver Brazier            | Silver Ingots, Coal      | 74.6% |
| Small Titanium Brazier    | Titanium Ingots, Coal    | 75.0% |
| Small Adamantine Brazier  | Adamantine Ingots, Coal  | 77.5% |
| Small Terathane Brazier   | Terathane Ingots, Coal   | 80.0% |
| Small Phoenix Brazier     | Phoenix Ingots, Coal     | 82.5% |
| Adamantine Brazier        | Adamantine Ingots, Coal  | 82.5% |
| Terathane Brazier         | Terathane Ingots, Coal   | 85.0% |

### Chests

| Item              | Resources            | Skills |
| ----------------- | -------------------- | ------ |
| Copper Chest      | Ingots, Nail, Hinges | 57.0%  |
| Pure Copper Chest | Ingots, Nail, Hinges | 58.0%  |
| Iron Chest        | Ingots, Nail, Hinges | 65.0%  |
| Ferrum Chest      | Ingots, Nail, Hinges | 69.0%  |
| Bronze Chest      | Ingots, Nail, Hinges | 70.0%  |
| Brass Chest       | Ingots, Nail, Hinges | 71.0%  |
| Steel Chest       | Ingots, Nail, Hinges | 72.5%  |
| Silver Chest      | Ingots, Nail, Hinges | 74.5%  |
| Titanium Chest    | Ingots, Nail, Hinges | 80%    |
| Adamantine Chest  | Ingots, Nail, Hinges | 82.5%  |
| Terathane Chest   | Ingots, Nail, Hinges | 85.0%  |
| Phoenix Chest     | Ingots, Nail, Hinges | 87.5%  |

### Golems

{{% notice note %}}
Golems mechanical creatures that players with the Tinkering skill can control.
They are un-shrinkable and are karma red so be careful where you use them.
{{% /notice %}}

| Golem      | Resources                                                                               | Tinkering to Craft | Tinkering to Control |
| ---------- | --------------------------------------------------------------------------------------- | ------------------ | -------------------- |
| Copper     | 30 Ingots, 8 Gears, 12 Hinges, 3 Strength Scrolls, 3 Agility Scrolls, 3 Cunning Scrolls | 75%                | 25%                  |
| Iron       | 30 Ingots, 8 Gears, 12 Hinges, 3 Strength Scrolls, 3 Agility Scrolls, 3 Cunning Scrolls | 80%                | 20%                  |
| Ferrum     | 30 Ingots, 8 Gears, 12 Hinges, 3 Strength Scrolls, 3 Agility Scrolls, 3 Cunning Scrolls | 80%                | 30%                  |
| Silver     | 30 Ingots, 8 Gears, 12 Hinges, 3 Strength Scrolls, 3 Agility Scrolls, 3 Cunning Scrolls | 86%                | 40%                  |
| Gold       | 30 Ingots, 8 Gears, 12 Hinges, 3 Strength Scrolls, 3 Agility Scrolls, 3 Cunning Scrolls | 86%                | 40%                  |
| Steel      | 30 Ingots, 8 Gears, 12 Hinges, 3 Strength Scrolls, 3 Agility Scrolls, 3 Cunning Scrolls | 87%                | 45%                  |
| Terathane  | 30 Ingots, 8 Gears, 12 Hinges, 3 Strength Scrolls, 3 Agility Scrolls, 3 Cunning Scrolls | 93%                | 50%                  |
| Titanium   | 30 Ingots, 8 Gears, 12 Hinges, 3 Strength Scrolls, 3 Agility Scrolls, 3 Cunning Scrolls | 91%                | 55%                  |
| Verite     | 30 Ingots, 8 Gears, 12 Hinges, 3 Strength Scrolls, 3 Agility Scrolls, 3 Cunning Scrolls | 91%                | 60%                  |
| Valorite   | 30 Ingots, 8 Gears, 12 Hinges, 3 Strength Scrolls, 3 Agility Scrolls, 3 Cunning Scrolls | 91%                | 60%                  |
| Verminard  | 30 Ingots, 8 Gears, 12 Hinges, 3 Strength Scrolls, 3 Agility Scrolls, 3 Cunning Scrolls | 93%                | 65%                  |
| Stronghold | 30 Ingots, 8 Gears, 12 Hinges, 3 Strength Scrolls, 3 Agility Scrolls, 3 Cunning Scrolls | 93%                | 65%                  |
| Blackrock  | 30 Ingots, 8 Gears, 12 Hinges, 3 Strength Scrolls, 3 Agility Scrolls, 3 Cunning Scrolls | 99%                | 70%                  |
| Adamantine | 30 Ingots, 8 Gears, 12 Hinges, 3 Strength Scrolls, 3 Agility Scrolls, 3 Cunning Scrolls | 99%                | 75%                  |
| Bloodrock  | 30 Ingots, 8 Gears, 12 Hinges, 3 Strength Scrolls, 3 Agility Scrolls, 3 Cunning Scrolls | 99%                | 80%                  |

### Tools and Miscellaneous Items

| Name                | Resources                                                                     | Skill |
| ------------------- | ----------------------------------------------------------------------------- | ----- |
| Tinkering Scissors  | Iron Ingots                                                                   | 14.5% |
| Sewing kit          | Iron Ingots                                                                   | 18.8% |
| Empty Bottle        | Iron Ingot                                                                    | 21.3% |
| Dice and Cup        | Pine Logs                                                                     | 30.0% |
| Game Board          | Pine Logs                                                                     | 30.0% |
| Backgammon Game     | Pine Logs                                                                     | 30.0% |
| Axel and Gears Axel | Iron Gears                                                                    | 30.4% |
| Axel                | Pine Log                                                                      | 30.4% |
| Clock Parts         | Axel and Gears, Iron Springs                                                  | 30.5% |
| Sextant             | Sextant parts                                                                 | 30.7% |
| Sextant parts       | Iron Ingots                                                                   | 31.0% |
| Saw                 | Iron Ingots, Pine Log                                                         | 31.3% |
| Hatchet             | Iron Ingots                                                                   | 33.9% |
| Tinker's Tool       | Iron Ingots                                                                   | 35.0% |
| Barrel Hoops        | Iron Ingots                                                                   | 42.0% |
| Ceramic Mug         | Iron Ingots                                                                   | 42.0% |
| Goblet              | Iron Ingots                                                                   | 42.0% |
| Goblet 2            | Iron Ingots                                                                   | 42.0% |
| Pewter Mug          | Iron Ingots                                                                   | 42.0% |
| Pitcher             | Iron Ingots                                                                   | 42.0% |
| Skull Mug           | Iron Ingots                                                                   | 42.0% |
| Spittoon            | Iron Ingots                                                                   | 42.0% |
| Pickaxe             | Iron Ingots, Pine Log                                                         | 42.1% |
| Lock Pick           | Iron Ingots                                                                   | 48.5% |
| Barrel Tap          | Iron Ingots                                                                   | 52.0% |
| Clock Frame         | Pine Logs, Iron Nail                                                          | 53.7% |
| Clock               | Clock Parts, Clock Frame                                                      | 55.1% |
| Anvil Parts         | Iron Ingots                                                                   | 60.0% |
| Forge Parts         | Iron Ingots, Hinges                                                           | 65.0% |
| Keg                 | 12 Pine Logs, 1 Barrel Tap, 2 Barrel Hoops, 36 Iron Nails, 1 Beeswax, 1 Hinge | ? %   |
| Hair Dye Tub        | 5 Pine Log, 5 Iron Nails                                                      | ? %   |
