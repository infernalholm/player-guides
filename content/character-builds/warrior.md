+++
title="Warrior"
date= 2018-10-30T20:13:42+00:00
weight=1
description= "Warrior. Perfect for PVP"
draft= false
+++

## Abilities

| Ability             | Score |
| ------------------- | ----- |
| Player vs Player    | 4/5   |
| Combat with Weapons | 4/5   |
| Combat with Mage    | 4/5   |
| House Fights        | 4/5   |
| Hunting             | 4/5   |

## Stats

| str                     | dex | int |
| ----------------------- | --- | --- |
| 120  (140 with Fluxes)  | 90  | 65  |

## Skills

| Skill          | Amount | Observations                                                                                 |
| -------------- | ------ | -------------------------------------------------------------------------------------------- |
| Archery        | 100%   | It comes in handy in combat situations.                                                      |
| Assessment     | 100%   | Adds bonuses in combat by increasing the chance to score critical hits.                      |
| Blade Weapons  | 100%   |                                                                                              |
| Healing        | 100%   | Used with Assessment.                                                                        |
| Hiding         | 100%   | Required                                                                                     |
| Magery         | 100%   | It will be your most used skill.                                                             |
| Meditation     | 100%   | Level your mana.                                                                             |
| Mystic Lore    | 100%   | Increases the effects of spell cast such as duration of fields and power of damaging spells. |
| Resist Magic   | 100%   | Protect yourself from magic spells                                                           |
| Shieldfighting | 100%   | Protect yourself from Combat Damage                                                          |
| Tactics        | 100%   | Hit the opponents more frequently with your weapons and fail less.                           |
| Stealth        | 70%    |                                                                                              |
| Detection      | 30%    | Reveal hidden players.                                                                       |
