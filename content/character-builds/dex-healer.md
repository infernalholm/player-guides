+++
title="Dex Healer"
date= 2018-10-30T20:13:42+00:00
weight=1
description= "Dex Healer. Heal faster than the damage you sustain."
draft= false
+++

## Abilities

| Ability             | Score |
| ------------------- | ----- |
| Player vs Player    | 3/5   |
| Combat with Weapons | 5/5   |
| Combat with Mage    | 1/5   |
| House Fights        | 5/5   |
| Hunting             | 4/5   |

## Stats

| str                     | dex | int |
| ----------------------- | --- | --- |
| 120 (140 with fluxes)   | 120 | 120 |


{{% notice tip %}}
High str will help your give more damage.
High dex will make you heal faster with bandages and faster hitting with
weapons.
{{% /notice %}}

## Skills

| Skill            | Amount | Observations                                                            |
| ---------------- | ------ |------------------------------------------------------------------------ |
| Archery          | 100%   | With high dex you will become fast.                                     |
| Assessment       | 100%   | Adds bonuses in combat by increasing the chance to score critical hits. |
| Piercing Weapons | 100%   | Fast weapons. Works well with Poisoning.                                |
| Healing          | 100%   | This is the whole point of this build.                                  |
| Hiding           | 100%   | Required for Stealth. Useful to keep you alive.                         |
| Magery           | 100%   |                                                                         |
| Poisoning        | 100%   | Works well with Piercing weapons                                        |
| Resist Magic     | 100%   | Protect yourself from magic spells.                                     |
| Shieldfighting   | 100%   | Protect yourself from Combat Damage.                                    |
| Tactics          | 100%   | Hit the opponents more frequently with your weapons and fail less.      |
| Tinkering        | 100%   | To use Golems in house fights.                                          |
| Stealth          | 70%    | Useful in house fights.                                                 |
| Detection        | 30%    | Useful in house fights.                                                 |
