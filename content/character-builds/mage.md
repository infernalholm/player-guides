+++
title="Mage"
date= 2018-10-30T20:13:42+00:00
weight=1
description= "Mage. Fight at a distance."
draft= false
+++

## Abilities

| Ability             | Score |
| ------------------- | ----- |
| Player vs Player    | 4/5   |
| Combat with Weapons | 0/5   |
| Combat with Mage    | 5/5   |
| House Fights        | 2/5   |
| Hunting             | 4/5   |

## Stats

| str   | dex | int                   |
| ----- | --- | --------------------- |
| 120   | 35  | 120 (140 with Fluxes  |

## Skills

| Skill        | Amount | Observations                                  |
| ------------ | ------ |---------------------------------------------- |
| Assessment   | 100%   | Needed for your bandages to heal you further. |
| Detection    | 100%   | Needed as much for farming or PvP.            |
| Healing      | 100%   | Main sources of healing, besides magery.      |
| Hiding       | 100%   | Needed as much for farming or PvP.            |
| Magery       | 100%   |                                               |
| Meditation   | 100%   |                                               |
| Musicianship | 100%   | Bard skill. Useful for farming.               |
| Mystic Lore  | 100%   |                                               |
| Necromancy   | 100%   | Useful for farming and PVP.                   |
| Resist Magic | 100%   |                                               |
| Songs of War | 100%   | Bard skill. Useful for farming.               |
| Stealth      | 100%   | Needed as much for farming or PvP.            |

{{% notice tip %}}
If you go as a hunter, you will explore the world. As you explore those places you might encounter Rare Steeds, Taming will come in handy.
{{% /notice %}}
