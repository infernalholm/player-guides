+++
title = "Character Builds"
date = 2018-11-01T14:44:30Z
weight = 1
chapter = true
+++

# Character Builds

- [Dex Healer]({{< ref "dex-healer.md" >}})
- [Mage]({{< ref "mage.md" >}})
- [Trader]({{< ref "trader.md" >}})
- [Warrior]({{< ref "warrior.md" >}})
