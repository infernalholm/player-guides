+++
title="Trader"
date= 2018-10-30T20:13:42+00:00
weight=1
description= "Trader. Craftsman"
draft= false
+++

## Abilities

| Ability             | Score |
| ------------------- | ----- |
| Player vs Player    | 2/5   |
| Combat with Weapons | 0/5   |
| Combat with Mage    | 3/5   |
| House Fights        | 0/5   |
| Hunting             | 1/5   |

## Stats

| str                     | dex | int  |
| ----------------------- | --- | ---- |
| 120  (140 with Fluxes)  | 35  | 120  |

## Skills

| Skill         | Amount | Observations                                                         |
| ------------- | ------ | -------------------------------------------------------------------- |
| Arms Lore     | 100%   | It improves the quality of the items you craft.                      |
| Alchemy       | 100%   | Standalone skill with no dependencies on other skills.               |
| Blacksmithy   | 100%   |                                                                      |
| Bowery        | 100%   | Requires Carpentry and Lumberjacking.                                |
| Carpentry     | 100%   | Required for Bowery.                                                 |
| Hiding        | 100%   | Not required. Helps to explore dangerous mines with great resources. |
| Lumberjacking | 100%   | Required for Carpentry and Bowery                                    |
| Magery        | 100%   | Required for Inscription, and to protect yourself.                   |
| Mining        | 100%   |                                                                      |
| Stealth       | 100%   | Not required. Helps to explore dangerous mines with great resources. |
| Tailoring     | 100%   | It improves the quality of the items you craft.                      |
| Inscription   | 100%   | Depends on Magery.                                                   |

