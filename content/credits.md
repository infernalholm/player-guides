---
title: "Credits"
date: 2018-11-01T16:09:14Z
draft: false
---

Special thanks to the wonderful people at MPZ, BurstfireUO (BFUO) and The UO
Project (TUP), who created and maintained the original version of this Ultima
Online server and player guides.

